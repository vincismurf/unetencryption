﻿/// <summary>
/// UNet LLAPI Hello World
/// This will establish a connection to a server socket from a client socket, then send serialized data and deserialize it.
/// </summary>

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System;



[Serializable]
public class Token
{
	public DateTime val;
	public int id;
}

[Serializable]
public class Message
{
	public string type;
	public string val;
	public Token token;
}

public class NetworkTest : MonoBehaviour {
	
	int mServerSocket = -1;
	int mClientSocket = -1;
	int mClientConnection = -1;
	int mMaxConnections = 10;
	byte mChannelUnreliable;
	byte mChannelReliable;
	bool mHostClientInitialized = false;
	bool mClientConnected = false;

	uint golbalConfigTimeout = 10;

	string hostIP = "127.0.0.1";
	int hostPort = 7777;
	int exceptionConnID = 0;

	string serverFailed = "Server socket creation failed!";
	string clientFailed = "Client socket creation failed!";
	string serverSuccess = "Server socket creation successful!";
	string clientSuccess = "Client socket creation successful!";
	//topology
	ConnectionConfig cc;
	HostTopology ht;
	//GlobalConfig
	GlobalConfig gc;

	//Encrypted 
	CryptoStream cs;

	Token [] serverTokens = new Token[10];
	Token clientToken;

	string tokenType = "Token";
	string testType = "Test";
	//Encryption 
	byte[] salt = new byte[] { 0x43, 0x87, 0x23, 0x72,0x42, 0x81, 0x23, 0x72};
	string password = "hjiweykaksd";
	// Use this for initialization
	void Start () {
		initConfig();
		initTopolgy();		
	}

	public void initHost()
	{
		// We have all of our other stuff figured out, so init
		NetworkTransport.Init(gc);
		
		// Open sockets for server and client
		mServerSocket = NetworkTransport.AddHost ( ht , hostPort  );

		// Check to make sure our socket formations were successful
		if( mServerSocket < 0 ){ Debug.Log (serverFailed); } else { Debug.Log (serverSuccess); }

		mHostClientInitialized = true;
	}
	void initConfig()
	{
		// Build global config
		gc = new GlobalConfig();
		gc.ReactorModel = ReactorModel.FixRateReactor;
		gc.ThreadAwakeTimeout = golbalConfigTimeout;

	}
	void initTopolgy()
	{
		// Build channel config
		cc = new ConnectionConfig();
		mChannelReliable = cc.AddChannel (QosType.ReliableSequenced);
		mChannelUnreliable = cc.AddChannel(QosType.UnreliableSequenced);

		// Create host topology from config
		ht = new HostTopology( cc , mMaxConnections  );
	}

	public void initClient()
	{
		if(!mHostClientInitialized)
			NetworkTransport.Init(gc);
		mClientSocket = NetworkTransport.AddHost ( ht  );
		if( mClientSocket < 0 ){ Debug.Log (clientFailed); } else { Debug.Log (clientSuccess); }

		// Connect to server
		byte error;
		mClientConnection = NetworkTransport.Connect( mClientSocket , hostIP , hostPort , exceptionConnID , out error );

		LogNetworkError( error );
		mHostClientInitialized = true;
	}


	
	public void SendTestData(){
		// Send the server a message
		byte error;

		byte[] buffer = new byte[1024];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();
		Token to = clientToken;

		Message mes = new Message();
		mes.token = to;
		mes.type = testType;
		mes.val = "Howdy";
		f.Serialize ( stream , mes );

		byte [] encryptedBytes = new byte[1024];
		encryptedBytes = Encrypt(buffer,(int)stream.Position);
		NetworkTransport.Send ( mClientSocket , mClientConnection , mChannelReliable , encryptedBytes , (int)encryptedBytes.Length , out error );

		LogNetworkError ( error );
	}
	
	/// <summary>
	/// Log any network errors to the console.
	/// </summary>
	/// <param name="error">Error.</param>
	void LogNetworkError(byte error){
		if( error != (byte)NetworkError.Ok){
			NetworkError nerror = (NetworkError)error;
			Debug.Log ("Error: " + nerror.ToString ());
		}
	}
	
	
	// Update is called once per frame
	void Update () {
		
		if(!mHostClientInitialized){
			return;
		}
		
		int recHostId; 
		int connectionId;
		int channelId;
		int dataSize;
		byte[] buffer = new byte[1024];
		byte error;
		
		NetworkEventType networkEvent = NetworkEventType.DataEvent;
		
		// Poll both server/client events
		do
		{
			networkEvent = NetworkTransport.Receive( out recHostId , out connectionId , out channelId , buffer , 1024 , out dataSize , out error );
			
			switch(networkEvent){
			case NetworkEventType.Nothing:
				break;
			case NetworkEventType.ConnectEvent:
				// Server received disconnect event
				if( recHostId == mServerSocket ){
					Debug.Log ("Server: Player " + connectionId.ToString () + " connected!" );
					setUpServerToken(connectionId);
				}else
				if( recHostId == mClientSocket ){
					Debug.Log ("Client: Client connected to " + connectionId.ToString () + "!" );
					// Set our flag to let client know that they can start sending data and send some data
					mClientConnected = true; 
					//SendTestData ();
				}else
				{
					Debug.Log("Server Error: Bad Token");

				}
				
				break;
				
			case NetworkEventType.DataEvent:
				// Server received data
				if( recHostId == mServerSocket ){
 

					byte[] decrypedBuffer = new byte[1024];
					decrypedBuffer = Decrypt(buffer, dataSize);
					// Let's decode data
					Stream stream = new MemoryStream(decrypedBuffer);
					BinaryFormatter f = new BinaryFormatter();
					//string msg = f.Deserialize( stream ).ToString ();

					Message mes = (Message)f.Deserialize(stream);

					if(isValidMessage(mes, connectionId)){
						//string msg = mes.type + " " +mes.val  + " " + mes.token.val;
						//Debug.Log ("Server: Received Data from " + connectionId.ToString () + "! Message: " +  msg   );
						Debug.Log ("Server: Received Data from Client " +  connectionId.ToString () + " :"+ mes.val +"!" );
					}
				}
				if( recHostId == mClientSocket ){
					Debug.Log ("Client: Received Data from Server!" );

					byte[] decrypedBuffer = new byte[1024];
					decrypedBuffer = Decrypt(buffer, dataSize);
					// Let's decode data
					Stream stream = new MemoryStream(decrypedBuffer);
					BinaryFormatter f = new BinaryFormatter();
					
					Message mes = (Message)f.Deserialize(stream);
					if(mes.type.Equals(tokenType))
					{
						Debug.Log("Client Received Token ");
						clientToken = mes.token;
					}else{
						if(clientToken == null)
						{
							Debug.Log ("Client Error: Token should be set");
						}
						if(clientToken.val == mes.token.val)
						{
							Debug.Log("Token Match: " + mes.val);
						}else{
							Debug.Log("Token do NOT match");
						}

					}

				}
				break;
				
			case NetworkEventType.DisconnectEvent:
				// Client received disconnect event
				if( recHostId == mClientSocket && connectionId == mClientConnection ){
					Debug.Log ("Client: Disconnected from server!");
					
					// Flag to let client know it can no longer send data
					mClientConnected = false;
				}
				
				// Server received disconnect event
				if( recHostId == mServerSocket ){
					Debug.Log ("Server: Received disconnect from " + connectionId.ToString () );
				}
				break;
			}
			
		} while ( networkEvent != NetworkEventType.Nothing );
	}

	bool isValidMessage (Message mes, int connectionId)
	{
		Token t = serverTokens[connectionId-1];
		return t.id == mes.token.id;
	}

	void setUpServerToken(int connectionId)
	{
		Token t = new Token();
		t.id = connectionId;
		t.val = DateTime.Now;
		serverTokens[connectionId-1]= t;
		sendTokenToClient(connectionId);
	}
	void sendTokenToClient(int connectionId)
	{
		// Send the server a message
		byte error;
		
		byte[] buffer = new byte[1024];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();

		Token t = serverTokens[connectionId-1];
		
		
		Message mes = new Message();
		mes.type = tokenType;
		mes.token = t;
		
		f.Serialize ( stream , mes );
		
		
		byte [] encryptedBytes = new byte[1024];
		encryptedBytes = Encrypt(buffer,(int)stream.Position);
		NetworkTransport.Send ( mServerSocket , connectionId , mChannelReliable , encryptedBytes , (int)encryptedBytes.Length , out error );
		
		LogNetworkError ( error );
	}
	public void sendMessageToClient()
	{
		// Send the server a message
		byte error;

		int connectionId = 1;// watch out this is not flexible
		
		byte[] buffer = new byte[1024];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();


		Token t = serverTokens[connectionId-1];

		Message mes = new Message();
		mes.type = testType;
		mes.token = t;
		mes.val = "Did this make it?";
		
		f.Serialize ( stream , mes );
		
		
		byte [] encryptedBytes = new byte[1024];
		encryptedBytes = Encrypt(buffer,(int)stream.Position);

		NetworkTransport.Send ( mServerSocket , connectionId , mChannelReliable , encryptedBytes , (int)encryptedBytes.Length , out error );
		
		LogNetworkError ( error );
	}
	byte[] Encrypt(byte[] input, int length)
	{
		Rfc2898DeriveBytes pdb =
			new Rfc2898DeriveBytes(password, // Change this
			                        salt); // Change this
		MemoryStream ms = new MemoryStream();
		Aes aes = new AesManaged();
		aes.Key = pdb.GetBytes(aes.KeySize / 8);
		aes.IV = pdb.GetBytes(aes.BlockSize / 8);
		cs = new CryptoStream(ms,
		                      aes.CreateEncryptor(), CryptoStreamMode.Write);
		
		cs.Write(input, 0, length);
		cs.Close();
		return ms.ToArray();
	}
	byte[] Decrypt(byte[] input, int length)
	{
		Rfc2898DeriveBytes pdb =
			new Rfc2898DeriveBytes(password, // Change this
			                        salt); // Change this
		MemoryStream ms = new MemoryStream();
		Aes aes = new AesManaged();
		aes.Key = pdb.GetBytes(aes.KeySize / 8);
		aes.IV = pdb.GetBytes(aes.BlockSize / 8);
		CryptoStream cs = new CryptoStream(ms,
		                                   aes.CreateDecryptor(), CryptoStreamMode.Write);
		cs.Write(input, 0, length);
		cs.Close();
		return ms.ToArray();
	}

	byte[] Encrypt(byte[] input)
	{
		Rfc2898DeriveBytes pdb =
			new Rfc2898DeriveBytes(password, // Change this
			                        salt); // Change this
		MemoryStream ms = new MemoryStream();
		Aes aes = new AesManaged();
		aes.Key = pdb.GetBytes(aes.KeySize / 8);
		aes.IV = pdb.GetBytes(aes.BlockSize / 8);
		cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write);

		cs.Write(input, 0, input.Length);
		cs.Close();
		return ms.ToArray();
	}

	byte[] Decrypt(byte[] input)
	{
		Rfc2898DeriveBytes pdb =
			new Rfc2898DeriveBytes(password, // Change this
			                        salt); // Change this

		MemoryStream ms = new MemoryStream();
		Aes aes = new AesManaged();
		aes.Key = pdb.GetBytes(aes.KeySize / 8);
		aes.IV = pdb.GetBytes(aes.BlockSize / 8);
		CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write);
		cs.Write(input, 0, input.Length);
		cs.Close();
		return ms.ToArray();
	}

	string Encrypt(string input)
	{
		return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(input)));

	}
	string Decrypt(string input)
	{
		return Encoding.UTF8.GetString(Decrypt(Convert.FromBase64String(input)));

	}
}
